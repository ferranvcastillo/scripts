#!/bin/bash
# Este script automatiza las Tweak Settings en cPanel & WHM
# Autor: Ferran Villalba Castillo
/bin/sed -i 's/gzip_compression_level=6/gzip_compression_level=5/g' /var/cpanel/cpanel.config
/bin/sed -i 's/allowremotedomains=0/allowremotedomains=1/g' /var/cpanel/cpanel.config
/bin/sed -i 's/create_account_dkim=1/create_account_dkim=0/g' /var/cpanel/cpanel.config
/bin/sed -i 's/proxysubdomainsfornewaccounts=1/proxysubdomainsfornewaccounts=0/g' /var/cpanel/cpanel.config
/bin/sed -i 's/publichtmlsubsonly=0/publichtmlsubsonly=1/g' /var/cpanel/cpanel.config
/bin/sed -i 's/skipboxtrapper=0/skipboxtrapper=1/g' /var/cpanel/cpanel.config
/bin/sed -i 's/skiphorde=0/skiphorde=1/g' /var/cpanel/cpanel.config
/bin/sed -i 's/skipmailman=0/skipmailman=1/g' /var/cpanel/cpanel.config
/bin/sed -i 's/skiproundcube=0/skiproundcube=1/g' /var/cpanel/cpanel.config
/bin/sed -i 's/skipspamassassin=0/skipspamassassin=1/g' /var/cpanel/cpanel.config
/bin/sed -i 's/skipsqmail=0/skipsqmail=1/g' /var/cpanel/cpanel.config
/bin/sed -i 's/skipdiskusage=0/skipdiskusage=1/g' /var/cpanel/cpanel.config
/bin/sed -i 's/skipbwlimitcheck=1/skipbwlimitcheck=0/g' /var/cpanel/cpanel.config
/bin/sed -i 's/phploader=/phploader=ioncube/g' /var/cpanel/cpanel.config
/bin/sed -i 's/use_information_schema=1/use_information_schema=0/g' /var/cpanel/cpanel.config
/bin/sed -i 's/cgihidepass=0/cgihidepass=1/g' /var/cpanel/cpanel.config
/bin/sed -i 's/cookieipvalidation=strict/cookieipvalidation=loose/g' /var/cpanel/cpanel.config
/bin/sed -i 's/requiressl=1/requiressl=0/g' /var/cpanel/cpanel.config
/bin/sed -i 's/skipanalog=0/skipanalog=1/g' /var/cpanel/cpanel.config
/bin/sed -i 's/skipwebalizer=0/skipwebalizer=1/g' /var/cpanel/cpanel.config
/bin/sed -i 's/maxmem=512/maxmem=1024/g' /var/cpanel/cpanel.config
/bin/sed -i 's/server_locale=en/server_locale=es_es/g' /var/cpanel/cpanel.config
/bin/sed -i 's/resetpass=1/resetpass=0/g' /var/cpanel/cpanel.config
/bin/sed -i 's/resetpass_sub=1/resetpass_sub=0/g' /var/cpanel/cpanel.config
/usr/local/cpanel/scripts/restartsrv_cpsrvd