Instalar Python 3 en CentOS 7:

$ sudo yum clean all ; sudo yum -y update ; sudo yum groupinstall -y "Development Tools"

$ sudo yum install -y https://centos7.iuscommunity.org/ius-release.rpm

$ sudo yum clean all ; sudo yum -y update

$ sudo yum install -y python36u python36u-libs python36u-devel python36u-pip

$ python3.6 -V

$ sudo yum install ansible

Backup:

$ sudo mv /etc/ansible/hosts /etc/ansible/hosts.bak

$ sudo vi /etc/ansible/hosts