#!/bin/bash
# Este script sencillo instala el software Maldet (LMD)
# Autor: Ferran Villalba Castillo
cd /usr/local/src
wget http://www.rfxn.com/downloads/maldetect-current.tar.gz
tar zxvf maldetect-current.tar.gz
cd maldetect-*
./install.sh