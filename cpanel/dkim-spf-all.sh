#!/bin/bash
# bash dkim-spf-all.sh

for username in `ls -A /var/cpanel/users`; do
    echo "Installing DKIM and SPF for $username"
    /usr/local/cpanel/bin/dkim_keys_install $username
    /usr/local/cpanel/bin/spf_installer $username
done